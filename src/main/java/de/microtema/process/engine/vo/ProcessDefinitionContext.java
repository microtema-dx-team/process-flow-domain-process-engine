package de.microtema.process.engine.vo;

import lombok.Builder;
import lombok.Data;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.Resource;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Process;

import java.util.function.Function;

@Data
@Builder
public class ProcessDefinitionContext {

    private Resource resource;

    private BpmnModelInstance bpmnModelInstance;

    public Function<Process, ProcessDefinition> processDefinition;
}
