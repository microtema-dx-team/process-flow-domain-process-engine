package de.microtema.process.engine.vo;

import de.microtema.process.engine.enums.ProcessStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ReportEvent {

    @NotNull
    private String activityId;

    @NotNull
    private String activityName;

    @NotNull
    private ProcessStatus processStatus;

    @NotNull
    private String processBusinessKey;

    private String description;

    @NotNull
    private LocalDateTime eventTime;
}
