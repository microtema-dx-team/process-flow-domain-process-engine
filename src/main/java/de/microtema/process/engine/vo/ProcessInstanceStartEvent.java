package de.microtema.process.engine.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

@Data
public class ProcessInstanceStartEvent {

    @NotEmpty
    private String processInstanceId;

    @NotNull
    Map<String, Object> variables = Collections.emptyMap();
    /**
     * Process definition Key is set of process-id, version and deployment-id
     */
    @NotNull
    private String definitionKey;
    @NotNull
    private String processName;

    @NotEmpty
    private String starterId;

    @NotEmpty
    private String referenceId;

    @NotEmpty
    private String referenceType;

    @NotNull
    private String referenceValue;

    @NotNull
    private String businessKey;
    @NotNull
    private String boundedContext;
    @NotNull
    private LocalDateTime startTime;
}