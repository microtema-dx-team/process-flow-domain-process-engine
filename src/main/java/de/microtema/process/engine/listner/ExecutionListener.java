package de.microtema.process.engine.listner;

import org.camunda.bpm.engine.delegate.DelegateExecution;

public interface ExecutionListener {

    boolean accept(String eventName);

    void notify(DelegateExecution delegateExecution);
}
