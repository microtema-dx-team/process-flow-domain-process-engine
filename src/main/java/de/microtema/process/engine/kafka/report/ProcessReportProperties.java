package de.microtema.process.engine.kafka.report;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "process-report")
public class ProcessReportProperties {

    private String topicName;
}
