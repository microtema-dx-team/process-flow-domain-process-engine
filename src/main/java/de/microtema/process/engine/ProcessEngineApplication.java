package de.microtema.process.engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ProcessEngineApplication {

    private static ApplicationContext context;

    public static void main(String[] args) {
        context = SpringApplication.run(ProcessEngineApplication.class, args);
    }

    public static ApplicationContext getContext(){

        return context;
    }
}
