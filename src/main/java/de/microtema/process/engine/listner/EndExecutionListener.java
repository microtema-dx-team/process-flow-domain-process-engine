package de.microtema.process.engine.listner;

import de.microtema.process.engine.converter.DelegateExecutionToReportEventConverter;
import de.microtema.process.engine.kafka.report.ProcessReportProducer;
import de.microtema.process.engine.vo.ReportEvent;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EndExecutionListener implements ExecutionListener {

    private final DelegateExecutionToReportEventConverter eventConverter;

    private final ProcessReportProducer processReportProducer;

    @Override
    public boolean accept(String eventName) {

        return StringUtils.equalsIgnoreCase(org.camunda.bpm.engine.delegate.ExecutionListener.EVENTNAME_END, eventName);
    }

    @Override
    public void notify(DelegateExecution delegateExecution) {

        ReportEvent reportEvent = eventConverter.convert(delegateExecution, "postDescription");

        processReportProducer.sendEvent(reportEvent);
    }
}
