package de.microtema.process.engine.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ProcessDefinitionDTO {

    @NotNull
    private String definitionKey;

    @NotNull
    private Integer definitionVersion;

    @NotNull
    private Integer majorVersion;

    @NotNull
    private String boundedContext;

    @NotNull
    private String processId;

    @NotNull
    private String displayName;

    private String description;

    @NotNull
    private String diagram;

    @NotNull
    private LocalDateTime deployTime;
}
