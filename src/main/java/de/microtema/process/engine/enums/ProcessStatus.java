package de.microtema.process.engine.enums;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Stream;

public enum ProcessStatus {

    STARTED("start"),
    ENDED("end"),
    WARNING("warning"),
    ERROR("error"),
    COMPLETED("completed");

    private final String value;

    ProcessStatus(String value) {
        this.value = value;
    }

    public static ProcessStatus finByValue(String value) {

        return Stream.of(values()).filter(it -> Objects.equals(it.value, value))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Unable to find element by value: " + value));
    }
}
