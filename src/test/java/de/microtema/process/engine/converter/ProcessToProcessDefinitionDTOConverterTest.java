package de.microtema.process.engine.converter;

import de.microtema.model.builder.annotation.Inject;
import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.process.engine.builder.BpmnModelInstanceModelBuilder;
import de.microtema.process.engine.vo.ProcessDefinitionContext;
import de.microtema.process.engine.vo.ProcessDefinitionDTO;
import org.camunda.bpm.engine.repository.Resource;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProcessToProcessDefinitionDTOConverterTest {

    @Inject
    ProcessToProcessDefinitionDTOConverter sut;

    @Inject
    BpmnModelInstanceModelBuilder instanceModelBuilder;

    @Mock
    Process process;

    @Mock
    ProcessDefinitionContext meta;

    @Mock
    org.camunda.bpm.engine.repository.ProcessDefinition processDefinition;

    @Mock
    Resource resource;

    @Model
    Integer camundaVersionTag;

    @Model
    Integer definitionVersion;

    @Model(resource = "bpmn/domain.bpmn")
    BpmnModelInstance bpmnModelInstance;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void convert() {

        when(process.getCamundaVersionTag()).thenReturn(camundaVersionTag.toString());
        when(processDefinition.getVersion()).thenReturn(definitionVersion);
        when(meta.getProcessDefinition()).thenReturn(it -> processDefinition);
        when(meta.getBpmnModelInstance()).thenReturn(bpmnModelInstance);
        when(meta.getResource()).thenReturn(resource);

        ProcessDefinitionDTO answer = sut.convert(process, meta);

        assertNotNull(answer);
        assertEquals(camundaVersionTag, answer.getMajorVersion());
        assertEquals(definitionVersion, answer.getDefinitionVersion());
    }
}
