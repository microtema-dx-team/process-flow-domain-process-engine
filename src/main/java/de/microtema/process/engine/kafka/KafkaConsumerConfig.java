package de.microtema.process.engine.kafka;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;

@Log4j2
@Configuration
public class KafkaConsumerConfig {

    @Bean
    public KafkaListenerErrorHandler kafkaListenerErrorHandler() {

        return (message, e) -> {

            Object payload = message.getPayload();

            log.error(() -> "Unable to handle event: " + payload, e);

            return payload;
        };
    }
}