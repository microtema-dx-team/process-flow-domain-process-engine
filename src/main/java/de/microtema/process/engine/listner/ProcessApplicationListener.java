package de.microtema.process.engine.listner;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@RequiredArgsConstructor
public class ProcessApplicationListener {

    private final Set<de.microtema.process.engine.listner.ExecutionListener> executionListeners;

    @EventListener
    public void notify(DelegateExecution delegateExecution) {

        String eventName = delegateExecution.getEventName();

        executionListeners.stream().filter(it -> it.accept(eventName)).forEach(it -> it.notify(delegateExecution));
    }
}
