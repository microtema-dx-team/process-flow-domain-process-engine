package de.microtema.process.engine.kafka.process;


import de.microtema.process.engine.converter.EventToProcessInstanceStartEventConverter;
import de.microtema.process.engine.service.ProcessInstanceService;
import de.microtema.process.engine.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessStartListener {

    private final ProcessInstanceService processInstanceService;

    private final StartProcessProperties startProcessProperties;

    private final EventToProcessInstanceStartEventConverter eventConverter;

    @SendTo("#{startProcessProperties.errorTopicName}")
    @KafkaListener(topics = "#{startProcessProperties.topicName}", errorHandler = "kafkaListenerErrorHandler")
    public void listen(@Payload String event) {

        log.info(() -> "Receive event: " + startProcessProperties.getTopicName());

        ProcessInstanceStartEvent processStartEvent = eventConverter.convert(event);

        processInstanceService.startProcess(processStartEvent);
    }
}