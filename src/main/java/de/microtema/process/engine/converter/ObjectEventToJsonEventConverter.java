package de.microtema.process.engine.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.model.converter.Converter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ObjectEventToJsonEventConverter implements Converter<String, Object> {

    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public String convert(Object orig) {

        return objectMapper.writeValueAsString(orig);
    }
}