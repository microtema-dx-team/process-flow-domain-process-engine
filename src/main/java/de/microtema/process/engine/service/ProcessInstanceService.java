package de.microtema.process.engine.service;

import de.microtema.process.engine.kafka.process.ProcessStartedProducer;
import de.microtema.process.engine.vo.ProcessDefinitionDTO;
import de.microtema.process.engine.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Log4j2
@Service
@RequiredArgsConstructor
public class ProcessInstanceService {

    private final RuntimeService runtimeService;
    private final ProcessStartedProducer processStartedProducer;
    private final ProcessDefinitionService processDefinitionService;

    public void startProcess(ProcessInstanceStartEvent processStartEvent) {

        String businessKey = processStartEvent.getBusinessKey();
        ProcessDefinitionDTO processDefinition = getProcessDefinition(processStartEvent);
        boolean runningInstance = existsRunningInstance(businessKey);

        if (runningInstance) {

            log.warn("Ignoring start-event, since there is already running one! " + businessKey);

            return;
        }

        startProcess(processDefinition, processStartEvent);
    }

    private void startProcess(ProcessDefinitionDTO processDefinition, ProcessInstanceStartEvent processStartEvent) {

        String processDefinitionKey = processDefinition.getProcessId();
        Map<String, Object> variables = processStartEvent.getVariables();
        String businessKey = processStartEvent.getBusinessKey();

        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey, businessKey, variables);

        String processInstanceId = processInstance.getProcessInstanceId();
        processStartEvent.setProcessInstanceId(processInstanceId);

        processStartedProducer.sendEvent(processStartEvent);
    }

    private ProcessDefinitionDTO getProcessDefinition(ProcessInstanceStartEvent processStartEvent) {

        String businessKey = processStartEvent.getBusinessKey();

        List<ProcessDefinitionDTO> processDefinitions = processDefinitionService.getProcessDefinitions();

        return processDefinitions.stream().filter(it -> match(it, processStartEvent))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unable to find process: " + businessKey));
    }

    private boolean match(ProcessDefinitionDTO processDefinitionDTO, ProcessInstanceStartEvent processStartEvent) {

        return Objects.equals(processDefinitionDTO.getDefinitionKey(), processStartEvent.getDefinitionKey());
    }

    public boolean existsRunningInstance(String businessKey) {

        List<ProcessInstance> processInstances =
                runtimeService.createProcessInstanceQuery()
                        .processInstanceBusinessKey(businessKey)
                        .active() // we only want the unsuspended process instances
                        .list();

        return !processInstances.isEmpty();
    }
}
