package de.microtema.process.engine.kafka.process;

import de.microtema.process.engine.converter.ObjectEventToJsonEventConverter;
import de.microtema.process.engine.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessStartedProducer {

    private final ObjectEventToJsonEventConverter eventConverter;

    private final StartedProcessProperties startedProcessProperties;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendEvent(ProcessInstanceStartEvent startEvent) {

        String topicName = startedProcessProperties.getTopicName();

        String event = eventConverter.convert(startEvent);

        log.info(() -> "fire event: " + topicName);

        kafkaTemplate.send(topicName, event);
    }
}
