package de.microtema.process.engine.converter;

import de.microtema.model.converter.MetaConverter;
import de.microtema.process.engine.enums.ProcessStatus;
import de.microtema.process.engine.vo.ReportEvent;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;

import static de.microtema.process.engine.utils.BpmnModelInstanceUtils.getExtensionPropertyValue;

@Component
public class DelegateExecutionToReportEventConverter implements MetaConverter<ReportEvent, DelegateExecution, String> {

    @Override
    public void update(ReportEvent dest, DelegateExecution orig, String meta) {

        dest.setActivityId(orig.getCurrentActivityId());
        dest.setActivityName(orig.getCurrentActivityName());

        dest.setProcessBusinessKey(orig.getProcessBusinessKey());
        dest.setEventTime(LocalDateTime.now());

        dest.setDescription(getDescription(orig, meta));
        dest.setProcessStatus(getProcessStatus(orig));
    }

    private String getDescription(DelegateExecution orig, String meta) {

        DelegateExecution processInstance = orig.getProcessInstance();
        FlowElement flowElement = processInstance.getBpmnModelElementInstance();

        if (Objects.isNull(flowElement)) {

            BpmnModelInstance bpmnModelInstance = processInstance.getBpmnModelInstance();

            String currentActivityId = orig.getCurrentActivityId();

            flowElement = bpmnModelInstance.getModelElementById(currentActivityId);
        }

        return getExtensionPropertyValue(flowElement, meta);
    }

    private ProcessStatus getProcessStatus(DelegateExecution orig) {

        ExecutionEntity executionEntity = (ExecutionEntity) orig;

        boolean ended = executionEntity.isEnded();

        if (ended) {
            return ProcessStatus.COMPLETED;
        } else {
            return ProcessStatus.finByValue(orig.getEventName());
        }
    }
}
