package de.microtema.process.engine.kafka.report;

import de.microtema.process.engine.converter.ObjectEventToJsonEventConverter;
import de.microtema.process.engine.vo.ReportEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessReportProducer {

    private final ObjectEventToJsonEventConverter eventConverter;

    private final ProcessReportProperties processReportProperties;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendEvent(ReportEvent startEvent) {

        System.out.println("sendEvent: " + startEvent.getActivityName() + ":" + startEvent.getProcessStatus());

        String topicName = processReportProperties.getTopicName();

        String event = eventConverter.convert(startEvent);

        log.info(() -> "fire event: " + topicName);

        kafkaTemplate.send(topicName, event);
    }
}
