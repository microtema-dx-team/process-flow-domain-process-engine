package de.microtema.process.engine.filter;

import de.microtema.process.engine.kafka.definition.ProcessDefinitionProducer;
import de.microtema.process.engine.service.ProcessDefinitionService;
import de.microtema.process.engine.vo.ProcessDefinitionDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Log4j2
@Component
@RequiredArgsConstructor
public class ProcessDefinitionRegistrationListener {

    private final ProcessDefinitionProducer processDefinitionProducer;

    private final ProcessDefinitionService processDefinitionService;

    @EventListener
    void onApplicationReadyEvent(ApplicationReadyEvent event) {

        List<ProcessDefinitionDTO> processDefinitions = processDefinitionService.getProcessDefinitions();

        processDefinitions.forEach(processDefinitionProducer::sendProcessDefinition);
    }
}
