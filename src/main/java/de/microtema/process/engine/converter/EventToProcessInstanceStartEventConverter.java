package de.microtema.process.engine.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.model.converter.Converter;
import de.microtema.process.engine.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EventToProcessInstanceStartEventConverter implements Converter<ProcessInstanceStartEvent, String> {

    private final ObjectMapper objectMapper;

    @SneakyThrows
    @Override
    public ProcessInstanceStartEvent convert(String orig) {

        return objectMapper.readValue(orig, ProcessInstanceStartEvent.class);
    }
}