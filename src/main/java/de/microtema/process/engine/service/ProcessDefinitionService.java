package de.microtema.process.engine.service;

import de.microtema.process.engine.converter.ProcessToProcessDefinitionDTOConverter;
import de.microtema.process.engine.vo.ProcessDefinitionContext;
import de.microtema.process.engine.vo.ProcessDefinitionDTO;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.Resource;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProcessDefinitionService {

    private final RepositoryService repositoryService;

    private final ManagementService managementService;

    private final ProcessToProcessDefinitionDTOConverter definitionConverter;

    public List<ProcessDefinitionDTO> getProcessDefinitions() {

        List<Resource> registeredDeployments = getRegisteredDeployments();

        return getProcessDefinitions(registeredDeployments);
    }

    private List<Resource> getRegisteredDeployments() {

        Set<String> registeredDeployments = managementService.getRegisteredDeployments();

        return registeredDeployments
                .stream()
                .map(repositoryService::getDeploymentResources)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<ProcessDefinitionDTO> getProcessDefinitions(Collection<Resource> resources) {

        return resources.stream()
                .map(this::getProcessDefinitions)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<ProcessDefinitionDTO> getProcessDefinitions(Resource resource) {

        BpmnModelInstance bpmnModelInstance = getBpmnModelInstance(resource);
        Collection<Process> executableProcesses = getExecutableProcesses(bpmnModelInstance);

        ProcessDefinitionContext definitionContext = ProcessDefinitionContext.builder()
                .resource(resource)
                .bpmnModelInstance(bpmnModelInstance)
                .processDefinition(it -> getProcessDefinition(resource, it))
                .build();

        return definitionConverter.convertList(executableProcesses, definitionContext);
    }

    private ProcessDefinition getProcessDefinition(Resource resource, Process process) {

        return repositoryService
                .createProcessDefinitionQuery()
                .deploymentId(resource.getDeploymentId())
                .processDefinitionKey(process.getId())
                .singleResult();
    }

    private BpmnModelInstance getBpmnModelInstance(Resource resource) {

        String deploymentId = resource.getDeploymentId();
        String name = resource.getName();

        InputStream inputStream = repositoryService.getResourceAsStream(deploymentId, name);

        return Bpmn.readModelFromStream(inputStream);
    }

    private Collection<Process> getExecutableProcesses(BpmnModelInstance bpmnModelInstance) {

        Collection<Process> process = bpmnModelInstance.getModelElementsByType(Process.class);

        return process.stream().filter(Process::isExecutable).collect(Collectors.toList());
    }
}
