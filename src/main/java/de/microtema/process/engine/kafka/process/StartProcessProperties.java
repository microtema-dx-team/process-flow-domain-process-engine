package de.microtema.process.engine.kafka.process;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "start-process")
public class StartProcessProperties {

    private String topicName;

    private String errorTopicName;
}
