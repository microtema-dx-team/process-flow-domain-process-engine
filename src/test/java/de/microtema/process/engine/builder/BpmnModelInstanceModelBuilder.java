package de.microtema.process.engine.builder;

import de.microtema.model.builder.ModelBuilder;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class BpmnModelInstanceModelBuilder implements ModelBuilder<BpmnModelInstance> {

    @Override
    public BpmnModelInstance fromResource(String resourceLocation) {

        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceLocation)) {

            return Bpmn.readModelFromStream(inputStream);
        } catch (IOException | NullPointerException e) {

            throw new IllegalArgumentException(e);
        }
    }
}
